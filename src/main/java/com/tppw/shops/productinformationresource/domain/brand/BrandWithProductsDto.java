package com.tppw.shops.productinformationresource.domain.brand;

import com.tppw.shops.productinformationresource.domain.model.brand.Brand;
import com.tppw.shops.productinformationresource.domain.model.product.Product;
import lombok.Value;

import java.util.Set;

@Value
public class BrandWithProductsDto {
    private final Long id;
    private final Set<String> names;
    private final Set<Product> products;

    public static BrandWithProductsDto of(Brand brand, Set<Product> products) {
        return new BrandWithProductsDto(brand.getId(), brand.getNames(), products);
    }
}
