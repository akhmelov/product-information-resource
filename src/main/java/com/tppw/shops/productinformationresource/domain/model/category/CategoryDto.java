package com.tppw.shops.productinformationresource.domain.model.category;

import lombok.Value;

import java.util.Set;

@Value
public class CategoryDto {
    private final CategoryIdDto id;
    private final CategoryIdDto parentId;
    private final Set<String> names;

    @Value
    public static class CategoryIdDto {
        private final String name;
        private final Category.CategoryId.Type type;

        public static CategoryIdDto of(Category.CategoryId id) {
            return new CategoryIdDto(id.getName(), id.getType());
        }

        public Category.CategoryId toDomain() {
            return new Category.CategoryId(name, type);
        }
    }
}
