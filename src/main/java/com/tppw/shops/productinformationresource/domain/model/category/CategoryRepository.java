package com.tppw.shops.productinformationresource.domain.model.category;

import com.tppw.shops.productinformationresource.domain.model.LanguageEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Category.CategoryId> {
    List<Category> findByNamesIgnoreCase(String name);
}
