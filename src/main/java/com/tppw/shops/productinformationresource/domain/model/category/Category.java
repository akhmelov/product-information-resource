package com.tppw.shops.productinformationresource.domain.model.category;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
public class Category {
    @EmbeddedId
    private CategoryId id;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "parent_name", referencedColumnName = "name"),
            @JoinColumn(name = "parent_type", referencedColumnName = "type")
    })
    @EqualsAndHashCode.Include
    @ToString.Include
    private Category parent;

    @ElementCollection
    @CollectionTable(name = "category_names", joinColumns = {
            @JoinColumn(name = "category_id_name", referencedColumnName = "name"),
            @JoinColumn(name = "category_id_type", referencedColumnName = "type")
    })
    @Column(name = "name")
    @EqualsAndHashCode.Include
    @ToString.Include
    private Set<String> names = new HashSet<>();

    @Data
    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CategoryId implements Serializable {
        public enum Type {FOOD, MEDICINE}

        private String name;
        @Enumerated(EnumType.STRING)
        private Type type;

        public CategoryDto.CategoryIdDto asDto() {
            return new CategoryDto.CategoryIdDto(name, type);
        }
    }

    public int voteForParentMatch(Set<String> parentsName) {
        if (parent == null) return 0;
        for (String name : parentsName) {
            if (parent.names.contains(name)) return parent.voteForParentMatch(parentsName) + 1;
        }
        return parent.voteForParentMatch(parentsName);
    }

    public Category(Category parent, List<String> names) {
        this.parent = parent;
        this.names.addAll(names);
    }

    public Category(CategoryId id, Category parent, List<String> names) {
        this.id = id;
        this.parent = parent;
        this.names.addAll(names);
    }

    public CategoryDto asDto() {
        final CategoryId parentId = Optional.ofNullable(parent).map(Category::getId).orElse(null);
        return new CategoryDto(id.asDto(), parentId.asDto(), new HashSet<>(names)); //todo possible null exception here
    }
}
