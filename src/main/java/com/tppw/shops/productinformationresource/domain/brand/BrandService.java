package com.tppw.shops.productinformationresource.domain.brand;

import com.tppw.shops.productinformationresource.domain.model.brand.Brand;
import com.tppw.shops.productinformationresource.domain.model.brand.BrandDto;
import com.tppw.shops.productinformationresource.domain.model.brand.BrandRepository;
import com.tppw.shops.productinformationresource.domain.model.product.Product;
import com.tppw.shops.productinformationresource.domain.model.product.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class BrandService {
    private final BrandRepository brandRepository;
    private final ProductRepository productRepository;

    public BrandDto putBrand(Set<String> brandNames) {
        final Brand brand = brandRepository
                .findByNamesInIgnoreCase(brandNames)
                .orElseGet(() -> brandRepository.save(new Brand(brandNames)));
        brand.addNamesIfNotExists(brandNames);
        return brand.asDto();
    }

    public Stream<BrandWithProductsDto> getBrandsWithProductsId() {
        return brandRepository.findAll().stream().map(brand -> BrandWithProductsDto.of(
                brand,
                new HashSet<>(productRepository.findAllByBrand(brand)))
        );
    }

    public void deleteName(long brandId, String name) {
        brandRepository.findById(brandId).ifPresent(brand -> brand.removeName(name));
    }
}
