package com.tppw.shops.productinformationresource.domain.model.brand;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "brand_names", joinColumns = @JoinColumn(name = "brand_id"))
    @Column(name = "name")
    private Set<String> names = new HashSet<>();

    public Brand(Set<String> names) {
        Objects.requireNonNull(names);
        this.names.addAll(names);
    }

    public Brand(List<String> name) {
        Objects.requireNonNull(name);
        this.names.addAll(name);
    }

    public boolean removeName(String name) {
        return names.remove(name);
    }

    public BrandDto asDto() {
        return new BrandDto(id, new HashSet<>(names));
    }

    public boolean addNamesIfNotExists(Set<String> brandNames) {
        return names.addAll(brandNames);
    }
}
