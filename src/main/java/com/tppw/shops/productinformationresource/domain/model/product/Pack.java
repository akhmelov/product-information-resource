package com.tppw.shops.productinformationresource.domain.model.product;

import com.tppw.shops.productinformationresource.domain.model.BarcodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import java.util.Map;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
class Pack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "pack_barcodes", joinColumns = @JoinColumn(name = "pack_id"))
    @MapKeyColumn(name = "barcode_type")
    @Column(name = "barcode_value")
    @MapKeyEnumerated(EnumType.STRING)
    @EqualsAndHashCode.Include
    private Map<BarcodeEnum, String> barcodes;

    public Pack(Map<BarcodeEnum, String> barcodes) {
        this.barcodes = barcodes;
    }

    public PackDto asDto() {
        return new PackDto(id, barcodes);
    }
}
