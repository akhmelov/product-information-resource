package com.tppw.shops.productinformationresource.domain.model.product;

import com.tppw.shops.productinformationresource.domain.model.BarcodeEnum;
import lombok.Value;

import java.util.Map;

@Value
public class PackDto {
    private final Long id;
    private final Map<BarcodeEnum, String> barcodes;
}
