package com.tppw.shops.productinformationresource.domain.model.brand;

import lombok.Value;

import java.util.Set;

@Value
public class BrandDto {
    private final Long id;
    private final Set<String> names;
}
