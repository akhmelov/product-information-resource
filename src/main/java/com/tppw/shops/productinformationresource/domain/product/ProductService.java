package com.tppw.shops.productinformationresource.domain.product;

import com.tppw.shops.productinformationresource.domain.category.CategoryService;
import com.tppw.shops.productinformationresource.domain.model.BarcodeEnum;
import com.tppw.shops.productinformationresource.domain.model.LanguageEnum;
import com.tppw.shops.productinformationresource.domain.model.category.Category;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryDto;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryRepository;
import com.tppw.shops.productinformationresource.domain.model.brand.Brand;
import com.tppw.shops.productinformationresource.domain.model.brand.BrandDto;
import com.tppw.shops.productinformationresource.domain.model.brand.BrandRepository;
import com.tppw.shops.productinformationresource.domain.model.product.Product;
import com.tppw.shops.productinformationresource.domain.model.product.Product.ProductDto;
import com.tppw.shops.productinformationresource.domain.model.product.ProductRepository;
import com.tppw.shops.productinformationresource.domain.brand.BrandService;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final BrandRepository brandRepository;
    private final CategoryService categoryService;
    private final BrandService brandService;

    @Transactional(readOnly = true)
    public Stream<ProductDto> getProducts() {
        return productRepository.findAll().stream().map(Product::asDto);
    }

    @Transactional(readOnly = true)
    public ProductDto getProduct(Long id) {
        return productRepository.findById(id).orElseThrow(ProductNotFoundException::new).asDto();
    }

    @Transactional
    public ProductDto putProduct(ProductCreateOrUpdateDto productCreateOrUpdateDto) {
        Category category = categoryRepository
                .findById(productCreateOrUpdateDto.getCategoryId().toDomain())
                .orElseThrow(CategoryNotFoundException::new);
        Brand brand = brandRepository.findById(productCreateOrUpdateDto.getBrandId()).orElseThrow(BrandNotFoundException::new);

        return putProduct(category, brand, productCreateOrUpdateDto.getPacksAsMapInSet(), productCreateOrUpdateDto.getNames());
    }

    @Transactional
    public ProductDto putPlainProduct(ProductCreateOrUpdatePlainDto productCreateOrUpdateDto) {
        final ProductCreateOrUpdatePlainDto.CategoryDto categoryFromRequest = productCreateOrUpdateDto.getCategory();
        final CategoryDto categoryDto = categoryService.findCategoryByNames(
                categoryFromRequest.getCategoryNames(),
                categoryFromRequest.getParentsNames()
        );
        final BrandDto brandDto = brandService.putBrand(productCreateOrUpdateDto.getBrandNames());

        Category category = categoryRepository
                .findById(categoryDto.getId().toDomain())
                .orElseThrow(CategoryNotFoundException::new);
        Brand brand = brandRepository.findById(brandDto.getId()).orElseThrow(BrandNotFoundException::new);

        return putProduct(category, brand, productCreateOrUpdateDto.getPacksAsMapInSet(), productCreateOrUpdateDto.getNames());
    }

    private ProductDto putProduct(Category category, Brand brand, Set<Map<BarcodeEnum, String>> packsAsMapInSet, Map<LanguageEnum, String> names) {
        final Optional<Product> product = findProduct(names, category, brand);
        if (product.isPresent()) {
            return product.get().update(
                    names,
                    packsAsMapInSet
            ).asDto();
        } else {
            return productRepository.save(Product.create(
                    category,
                    brand,
                    names,
                    packsAsMapInSet
            )).asDto();
        }
    }

    private Optional<Product> findProduct(Map<LanguageEnum, String> names, Category category, Brand brand) {
        return names.entrySet().stream().flatMap(entry ->
                productRepository.findByCategoryAndBrandAndNamesLanguageAndNamesNameIgnoreCase(
                        category,
                        brand,
                        entry.getKey(),
                        entry.getValue()
                ).map(Stream::of).orElseGet(Stream::empty))
                .findAny();
    }

    @Transactional(readOnly = true)
    public Stream<ProductDto> getProductsWithIds(Set<Long> productsId) {
        return productRepository.findByIdIn(productsId).stream().map(Product::asDto);
    }

    @Value
    public static class ProductCreateOrUpdateDto {
        private final CategoryId categoryId;
        private final Long brandId;

        private final Map<LanguageEnum, String> names;
        private final Set<Pack> packs;

        @Value
        public static class Pack {
            @NotNull
            private final Map<BarcodeEnum, String> barcodes;
        }

        @Value
        public static class CategoryId {
            private final String name;
            private final Category.CategoryId.Type type;

            public Category.CategoryId toDomain() {
                return new Category.CategoryId(name, type);
            }
        }

        Set<Map<BarcodeEnum, String>> getPacksAsMapInSet() {
            return packs.stream().map(Pack::getBarcodes).collect(Collectors.toSet());
        }
    }

    @Value
    public static class ProductCreateOrUpdatePlainDto {
        private final CategoryDto category;
        private final Set<String> brandNames;

        private final Map<LanguageEnum, String> names;
        private final Set<Pack> packs;

        @Value
        public static class CategoryDto {
            private final Set<String> categoryNames;
            private final Set<String> parentsNames;
        }

        @Value
        public static class Pack {
            @NotNull
            private final Map<BarcodeEnum, String> barcodes;
        }

        Set<Map<BarcodeEnum, String>> getPacksAsMapInSet() {
            return packs.stream().map(Pack::getBarcodes).collect(Collectors.toSet());
        }
    }

    public static class CategoryNotFoundException extends RuntimeException {
    }

    public static class BrandNotFoundException extends RuntimeException {
    }

    public static class ProductNotFoundException extends RuntimeException {
    }
}
