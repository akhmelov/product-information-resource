package com.tppw.shops.productinformationresource.domain.model.product;

import com.tppw.shops.productinformationresource.domain.model.BarcodeEnum;
import com.tppw.shops.productinformationresource.domain.model.LanguageEnum;
import com.tppw.shops.productinformationresource.domain.model.category.Category;
import com.tppw.shops.productinformationresource.domain.model.brand.Brand;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Brand brand;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Pack> packs = new HashSet<>();

    @ElementCollection
    @CollectionTable(name = "products_names", joinColumns = @JoinColumn(name = "product_id"))
    @MapKeyColumn(name = "language")
    @MapKeyEnumerated(EnumType.STRING)
    @Column(name = "name")
    private Map<LanguageEnum, String> names = new HashMap<>();

    public Product(Category category, Brand brand, String name, LanguageEnum language) {
        this.category = category;
        this.brand = brand;
        this.names.put(language, name);
    }

    public PackDto addPack(Map<BarcodeEnum, String> barcodes) {
        final Pack pack = new Pack(barcodes);
        packs.add(pack);
        return pack.asDto();
    }

    public void putName(LanguageEnum language, String name) {
        names.put(language, name);
    }

    public ProductDto asDto() {
        final Set<PackDto> packsId = CollectionUtils.emptyIfNull(packs).stream().map(Pack::asDto).collect(Collectors.toSet());
        return new ProductDto(id, category.getId().asDto(), brand.getId(), packsId, new HashMap<>(names));
    }

    public static Product create(Category category, Brand brand, Map<LanguageEnum, String> names, Set<Map<BarcodeEnum, String>> packs) {
        final Set<Pack> packsNew = packs.stream().map(Pack::new).collect(Collectors.toSet());
        return Product.builder()
                .category(category)
                .brand(brand)
                .names(names)
                .packs(packsNew)
                .build();
    }

    public Product update(Map<LanguageEnum, String> names, Set<Map<BarcodeEnum, String>> packs) {
        this.names.putAll(names);

        final Set<Pack> packsNew = packs.stream().map(Pack::new).collect(Collectors.toSet());
        this.packs.addAll(packsNew);
        return this;
    }

    @Value
    public static class ProductDto {
        private final Long id;
        private final CategoryDto.CategoryIdDto categoryId;
        private final Long brandId;
        private final Set<PackDto> packs;

        private final Map<LanguageEnum, String> names;
    }
}
