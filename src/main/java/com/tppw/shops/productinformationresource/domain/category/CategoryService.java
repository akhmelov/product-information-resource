package com.tppw.shops.productinformationresource.domain.category;

import com.tppw.shops.productinformationresource.domain.model.category.Category;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryDto;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class CategoryService {
    private final CategoryRepository categoryRepository;

    @Transactional
    public CategoryDto findCategoryByName(final String categoryName, Set<String> parentsName) {
        final List<Category> categories = categoryRepository.findByNamesIgnoreCase(categoryName);

        return categories.stream()
                .max(Comparator.comparingInt(c -> c.voteForParentMatch(parentsName)))
                .map(Category::asDto)
                .orElseThrow(() -> new NoSuchCategoryException(categoryName, parentsName));
    }

    @Transactional
    public CategoryDto findCategoryByNames(final Set<String> categoryNames, final Set<String> parentsNames) {
        return categoryNames
                .stream()
                .map(categoryName -> findCategoryByName(categoryName, parentsNames))
                .findAny()
                .orElseThrow(() -> new NoSuchCategoryException(categoryNames, parentsNames));
    }

    public static class NoSuchCategoryException extends RuntimeException {
        public NoSuchCategoryException(String categoryName, Set<String> parentCategoryNames) {
            this(Collections.singleton(categoryName), parentCategoryNames);
        }

        public NoSuchCategoryException(Set<String> categoryNames, Set<String> parentCategoryNames) {
            super(String.format("No category with names '%s' with parents '%s'", categoryNames, parentCategoryNames));
        }
    }
}
