package com.tppw.shops.productinformationresource.domain.model;

public enum BarcodeEnum {
    EAN, EAN_13, OTHER, INNER
}
