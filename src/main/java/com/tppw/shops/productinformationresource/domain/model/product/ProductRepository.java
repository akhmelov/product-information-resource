package com.tppw.shops.productinformationresource.domain.model.product;

import com.tppw.shops.productinformationresource.domain.model.LanguageEnum;
import com.tppw.shops.productinformationresource.domain.model.category.Category;
import com.tppw.shops.productinformationresource.domain.model.brand.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select p " +
            "from Product p join p.names n " +
            "where p.category = :category AND p.brand = :brand AND KEY(n) = :language AND :name IN (VALUE(n))")
    Optional<Product> findByCategoryAndBrandAndNamesLanguageAndNamesNameIgnoreCase(
            @Param("category") Category category,
            @Param("brand") Brand brand,
            @Param("language") LanguageEnum language,
            @Param("name") String name
    );

    List<Product> findAllByBrand(Brand brand);

    List<Product> findByIdIn(Set<Long> productsId);
}
