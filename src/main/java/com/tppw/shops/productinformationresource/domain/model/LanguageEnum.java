package com.tppw.shops.productinformationresource.domain.model;

public enum LanguageEnum {
    en, ru, pl
}
