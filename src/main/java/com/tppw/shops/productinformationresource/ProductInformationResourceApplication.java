package com.tppw.shops.productinformationresource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductInformationResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductInformationResourceApplication.class, args);
    }
}
