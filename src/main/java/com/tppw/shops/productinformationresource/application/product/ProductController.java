package com.tppw.shops.productinformationresource.application.product;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tppw.shops.productinformationresource.domain.model.BarcodeEnum;
import com.tppw.shops.productinformationresource.domain.model.LanguageEnum;
import com.tppw.shops.productinformationresource.domain.model.category.Category;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryDto;
import com.tppw.shops.productinformationresource.domain.model.product.PackDto;
import com.tppw.shops.productinformationresource.domain.model.product.Product.ProductDto;
import com.tppw.shops.productinformationresource.domain.product.ProductService;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/products")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public List<GetProductResponse> getProducts() {
        return productService.getProducts().map(GetProductResponse::create).collect(Collectors.toList());
    }

    @GetMapping(params = "productsId")
    public List<GetProductResponse> getProductsWithIds(@RequestParam Set<Long> productsId) {
        return productService.getProductsWithIds(productsId).map(GetProductResponse::create).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public GetProductResponse getProduct(@PathVariable Long id) {
        return GetProductResponse.create(productService.getProduct(id));
    }

    @PostMapping
    public CreateOrUpdateResponse createOrUpdate(@RequestBody @Valid CreateOrUpdateRequest createOrUpdateRequest) {
        return CreateOrUpdateResponse.create(productService.putProduct(createOrUpdateRequest.asDto()));
    }

    @PostMapping(params = "plain")
    public CreateOrUpdateResponse createOrUpdateByPlain(@RequestBody @Valid CreateOrUpdatePlainRequest createOrUpdatePlainRequest) {
        return CreateOrUpdateResponse.create(productService.putPlainProduct(createOrUpdatePlainRequest.asDto()));
    }

    @Value
    static class GetProductResponse {
        private final Long id;
        private final CategoryId categoryId;
        private final Long brandId;
        private final Map<LanguageEnum, String> names;
        private final Set<Pack> packs;

        @Value
        static class Pack {
            private final Long id;
            private final Map<BarcodeEnum, String> barcodes;

            static Pack create(PackDto pack) {
                return new Pack(pack.getId(), pack.getBarcodes());
            }
        }

        @Value
        static class CategoryId {
            private final String name;
            private final Category.CategoryId.Type type;

            static CategoryId create(CategoryDto.CategoryIdDto id) {
                return new CategoryId(id.getName(), id.getType());
            }
        }

        static GetProductResponse create(ProductDto product) {
            return new GetProductResponse(
                    product.getId(),
                    CategoryId.create(product.getCategoryId()),
                    product.getBrandId(),
                    product.getNames(),
                    product.getPacks().stream().map(Pack::create).collect(Collectors.toSet())
            );
        }
    }

    @Value
    static class CreateOrUpdateRequest {
        @NotNull
        private final CategoryId categoryId;
        @NotNull
        private final Long brandId;

        @NotEmpty
        private final Map<LanguageEnum, String> names;
        @Valid
        private final Set<Pack> packs;

        @Value
        static class Pack {
            @NotEmpty
            private final Map<BarcodeEnum, String> barcodes;

            @JsonCreator
            public Pack(@JsonProperty("barcodes") Map<BarcodeEnum, String> barcodes) {
                this.barcodes = barcodes;
            }

            ProductService.ProductCreateOrUpdateDto.Pack asDto() {
                return new ProductService.ProductCreateOrUpdateDto.Pack(barcodes);
            }
        }

        @Value
        static class CategoryId {
            private final String name;
            private final Category.CategoryId.Type type;

            ProductService.ProductCreateOrUpdateDto.CategoryId asDto() {
                return new ProductService.ProductCreateOrUpdateDto.CategoryId(name, type);
            }
        }

        ProductService.ProductCreateOrUpdateDto asDto() {
            return new ProductService.ProductCreateOrUpdateDto(
                    categoryId.asDto(),
                    brandId,
                    names,
                    packs.stream().map(Pack::asDto).collect(Collectors.toSet())
            );
        }
    }

    @Value
    static class CreateOrUpdatePlainRequest {
        @Valid
        @NotNull
        private final Category category;
        @NotEmpty
        private final Set<String> brandNames;

        @NotEmpty
        private final Map<LanguageEnum, String> names;
        @Valid
        private final Set<Pack> packs;

        @Value
        static class Category {
            @NotEmpty
            private final Set<String> categoryNames;
            private final Set<String> parentsNames;

            ProductService.ProductCreateOrUpdatePlainDto.CategoryDto asDto() {
                return new ProductService.ProductCreateOrUpdatePlainDto.CategoryDto(categoryNames, parentsNames);
            }
        }

        @Value
        static class Pack {
            @NotEmpty
            private final Map<BarcodeEnum, String> barcodes;

            @JsonCreator
            public Pack(@JsonProperty("barcodes") Map<BarcodeEnum, String> barcodes) {
                this.barcodes = barcodes;
            }

            ProductService.ProductCreateOrUpdatePlainDto.Pack asDto() {
                return new ProductService.ProductCreateOrUpdatePlainDto.Pack(barcodes);
            }
        }

        ProductService.ProductCreateOrUpdatePlainDto asDto() {
            return new ProductService.ProductCreateOrUpdatePlainDto(
                    category.asDto(),
                    brandNames,
                    names,
                    packs.stream().map(Pack::asDto).collect(Collectors.toSet())
            );
        }
    }

    @Value
    static class CreateOrUpdateResponse {
        private final Long id;
        private final CategoryId categoryId;
        private final Long brandId;
        private final Map<LanguageEnum, String> names;
        private final Set<Pack> packs;

        @Value
        static class Pack {
            private final Long id;
            private final Map<BarcodeEnum, String> barcodes;

            static Pack create(PackDto pack) {
                return new Pack(pack.getId(), pack.getBarcodes());
            }
        }

        @Value
        static class CategoryId {
            private final String name;
            private final Category.CategoryId.Type type;

            static CategoryId create(CategoryDto.CategoryIdDto id) {
                return new CategoryId(id.getName(), id.getType());
            }
        }

        static CreateOrUpdateResponse create(ProductDto product) {
            return new CreateOrUpdateResponse(
                    product.getId(),
                    CategoryId.create(product.getCategoryId()),
                    product.getBrandId(),
                    product.getNames(),
                    product.getPacks().stream().map(Pack::create).collect(Collectors.toSet())
            );
        }
    }
}
