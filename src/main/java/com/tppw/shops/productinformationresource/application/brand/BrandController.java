package com.tppw.shops.productinformationresource.application.brand;

import com.tppw.shops.productinformationresource.domain.brand.BrandWithProductsDto;
import com.tppw.shops.productinformationresource.domain.model.brand.BrandDto;
import com.tppw.shops.productinformationresource.domain.brand.BrandService;
import com.tppw.shops.productinformationresource.domain.model.product.Product;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequestMapping("/brands")
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BrandController {
    private final BrandService brandService;

    @GetMapping
    public List<BrandResponse> getBrands() {
        return brandService.getBrandsWithProductsId().map(BrandResponse::create).collect(Collectors.toList());
    }

    @DeleteMapping("/{brandId}/names/{name}")
    public void deleteNameInBrand(@PathVariable long brandId, @PathVariable String name) {
        brandService.deleteName(brandId, name);
    }

    @PutMapping("/{brandName}")
    public PutBrandResponse putBrand(@PathVariable String brandName, @RequestParam(required = false) Set<String> alternativeNames) {
        if (alternativeNames != null) {
            alternativeNames.add(brandName);
            return PutBrandResponse.create(brandService.putBrand(alternativeNames));
        } else {
            return PutBrandResponse.create(brandService.putBrand(Collections.singleton(brandName)));
        }
    }

    @Value
    static class BrandResponse {
        private final Long id;
        private final Set<String> names;
        private final Set<Long> productsId;

        static BrandResponse create(BrandWithProductsDto brand) {
            return new BrandResponse(
                    brand.getId(),
                    brand.getNames(),
                    brand.getProducts().stream().mapToLong(Product::getId).boxed().collect(Collectors.toSet())
            );
        }
    }

    @Value
    static class PutBrandResponse {
        private final Long id;
        private final Set<String> names;

        static PutBrandResponse create(BrandDto brand) {
            return new PutBrandResponse(brand.getId(), brand.getNames());
        }
    }
}
