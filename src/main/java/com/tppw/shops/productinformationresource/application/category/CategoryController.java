package com.tppw.shops.productinformationresource.application.category;

import com.tppw.shops.productinformationresource.domain.category.CategoryService;
import com.tppw.shops.productinformationresource.domain.model.category.Category;
import com.tppw.shops.productinformationresource.domain.model.category.CategoryDto;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Set;

@RestController
@AllArgsConstructor
@RequestMapping("/categories")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/{categoryName}")
    public GetCategoryResponse getCategory(@PathVariable String categoryName) {
        return GetCategoryResponse.create(categoryService.findCategoryByName(categoryName, Collections.emptySet()));
    }

    @Value
    static class GetCategoryResponse {
        private final CategoryId id;
        private final Set<String> names;
        private final CategoryId parentId;

        static GetCategoryResponse create(CategoryDto category) {
            return new GetCategoryResponse(
                    CategoryId.create(category.getId()),
                    category.getNames(),
                    CategoryId.create(category.getParentId())
            );
        }

        @Value
        static class CategoryId {
            private final String name;
            private final Category.CategoryId.Type type;

            static CategoryId create(CategoryDto.CategoryIdDto id) {
                return new CategoryId(id.getName(), id.getType());
            }
        }
    }
}
