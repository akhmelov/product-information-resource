CREATE TABLE category(
  name character varying(255) NOT NULL,
  type character varying(25) NOT NULL,
  parent_name character varying(255),
  parent_type character varying(25),
  PRIMARY KEY (name, type),
  CONSTRAINT fk_category_category_parent FOREIGN KEY (parent_name, parent_type)
    REFERENCES category (name, type) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE category_names(
  category_id_name character varying(255) NOT NULL,
  category_id_type character varying(25) NOT NULL,
  name character varying(255),
  PRIMARY KEY (category_id_name, category_id_type, name),
  CONSTRAINT fk_category_names_category_alternative_names FOREIGN KEY (category_id_name, category_id_type)
    REFERENCES category (name, type)  MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE brand(
  id BIGSERIAL PRIMARY KEY
);

CREATE TABLE brand_names
(
  brand_id bigint NOT NULL,
  name character varying(255) COLLATE pg_catalog."default",
  CONSTRAINT fk_brand_names_brand_alternative_names FOREIGN KEY (brand_id)
    REFERENCES brand (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE pack(
  id BIGSERIAL PRIMARY KEY
);

CREATE TABLE pack_barcodes(
  pack_id bigint NOT NULL,
  barcode_value character varying(255),
  barcode_type character varying(12) NOT NULL,
  CONSTRAINT pack_pack_barcodes_barcodes_pkey PRIMARY KEY (pack_id, barcode_type),
  CONSTRAINT fk_pack_barcodes FOREIGN KEY (pack_id)
    REFERENCES pack (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE product(
  id BIGSERIAL PRIMARY KEY,
  category_name character varying(255),
  category_type character varying(25),
  brand_id bigint,
  CONSTRAINT fk_product_category_category FOREIGN KEY (category_name, category_type)
    REFERENCES category (name, type) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
  CONSTRAINT fk_product_brand_brand FOREIGN KEY (brand_id)
    REFERENCES brand (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE product_packs(
  product_id bigint NOT NULL,
  packs_id bigint NOT NULL,
  CONSTRAINT product_packs_pkey PRIMARY KEY (product_id, packs_id),
  CONSTRAINT uk_pack_id UNIQUE (packs_id)
  ,
  CONSTRAINT fk_product_packs_product_product FOREIGN KEY (product_id)
    REFERENCES product (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
  CONSTRAINT fk_product_packs_pack_pack FOREIGN KEY (packs_id)
    REFERENCES pack (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);

CREATE TABLE products_names(
  product_id bigint NOT NULL,
  name character varying(255),
  language character varying(2) NOT NULL,
  CONSTRAINT products_names_pkey PRIMARY KEY (product_id, language),
  CONSTRAINT fk_products_names_product FOREIGN KEY (product_id)
    REFERENCES product (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
)
