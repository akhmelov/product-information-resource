TRUNCATE TABLE category CASCADE;
TRUNCATE TABLE category_names CASCADE;
TRUNCATE TABLE brand CASCADE;
TRUNCATE TABLE brand_names CASCADE;
TRUNCATE TABLE pack CASCADE;
TRUNCATE TABLE pack_barcodes CASCADE;
TRUNCATE TABLE product CASCADE;
TRUNCATE TABLE product_packs CASCADE;
TRUNCATE TABLE products_names CASCADE;
