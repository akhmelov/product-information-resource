//package com.tppw.shops.productinformationresource.tag
//
//import com.fasterxml.jackson.databind.ObjectMapper
//
//import org.junit.Before
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.test.web.servlet.MockMvc
//import org.springframework.test.web.servlet.setup.MockMvcBuilders
//import org.springframework.web.context.WebApplicationContext
//import spock.lang.Specification
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class TagIntegrationSpec extends Specification {
//    @Autowired
//    protected WebApplicationContext webApplicationContext
//
//    ObjectMapper objectMapper = new ObjectMapper()
//    private MockMvc mockMvc
//
//    private String tagName = "organic"
//
//    @Before
//    void setupMockMvc(){
//        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
//    }
//
//    def "should return tag"() {
//        when:
//            def result = mockMvc.perform(get("/tags/$tagName")).andReturn().response
//
//        then:
//            result.status == 200
//
//        and:
//            with (objectMapper.readValue(result.contentAsString, Map)) {
//                it.name == 'organic'
//            }
//    }
//
//    def "should return tag when search in PL"() {
//        when:
//            def result = mockMvc.perform(get("/tags/organiczny?language=PL")).andReturn().response
//
//        then:
//            result.status == 200
//
//        and:
//            with (objectMapper.readValue(result.contentAsString, Map)) {
//                it.name == 'organic'
//            }
//    }
//}
