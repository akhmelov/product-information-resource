package com.tppw.shops.productinformationresource.application.product

import com.tppw.shops.productinformationresource.domain.model.BarcodeEnum
import com.tppw.shops.productinformationresource.domain.model.LanguageEnum
import com.tppw.shops.productinformationresource.domain.model.category.Category
import com.tppw.shops.productinformationresource.domain.model.category.CategoryRepository
import com.tppw.shops.productinformationresource.domain.model.brand.Brand
import com.tppw.shops.productinformationresource.domain.model.brand.BrandRepository
import com.tppw.shops.productinformationresource.domain.model.product.Product
import com.tppw.shops.productinformationresource.domain.model.product.ProductRepository
import groovy.json.JsonSlurper
import org.apache.groovy.json.internal.LazyMap
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

import static groovy.json.JsonOutput.toJson
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:clearDB.sql")
@Testcontainers
class ProductIntegrationSpec extends Specification {

    @Autowired
    protected WebApplicationContext webApplicationContext

    private MockMvc mockMvc

    @Autowired
    private CategoryRepository categoryRepository

    @Autowired
    private BrandRepository brandRepository

    @Autowired
    private ProductRepository productRepository

    @Shared
    PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer()
            .withDatabaseName("product-information-test")
            .withUsername("foo")
            .withPassword("secret")

    def setupSpec() {
        ['spring.datasource.url'     : postgreSQLContainer.getJdbcUrl(),
         'spring.datasource.username': postgreSQLContainer.getUsername(),
         'spring.datasource.password': postgreSQLContainer.getPassword()
        ].each { k, v ->
            System.setProperty(k, v)
        }
    }

    @Before
    void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    def "should create & get product by name, language, category and brand"() {
        given: "categories"
        Category allFoods = withCategory(category('All Foods', 'FOOD', ["All Foods"]))
        Category landAnimals = withCategory(category('Land Animals', ["Land Animals"], allFoods.id))
        Category meatPoultry = withCategory(category('Meat-Poultry', ["Meat-Poultry"], landAnimals.id))
        Category meat = withCategory(category('Meat', ["Meat"], meatPoultry.id))
        withCategory(category('Beef', ["Beef"], meat.id))
        Category pork = withCategory(category('Pork', ["Pork", "Wieprzowina", "Świnina"], meat.id))
        and: "brands"
        Brand cracow = withBrand(brand(["Kraków", "Cracow"]))
        and: "product"
        Product testedProduct = product(
                [en: "product name in english", pl: "Product po polsku"],
                [[EAN: "ean-code-1"], [EAN: "ean-code-2"]],
                pork,
                cracow
        )

        when: "create product"
        def createCategoryByNameRequest = invokePost("/products", asRequestBody(testedProduct))
        Map createResponse = contentAsMap(createCategoryByNameRequest)
        then:
        httpStatus(createCategoryByNameRequest) == 200 //todo status creation
        createResponse.id ==~ /.*/
        createResponse.categoryId as Category.CategoryId == testedProduct.category.id
        createResponse.brandId == testedProduct.brand.id
        toLanguageMap(createResponse.names) == testedProduct.names
        asMapInSortedList(createResponse.packs*.barcodes) == asMapInSortedList(testedProduct.packs*.barcodes)

        when: "get product"
        def findCategoryByNameRequest = invokePost("/products", asRequestBody(testedProduct))
        Map findResponse = contentAsMap(findCategoryByNameRequest)
        then:
        httpStatus(findCategoryByNameRequest) == 200
        findResponse.id ==~ /.*/
        findResponse.categoryId as Category.CategoryId == testedProduct.category.id
        findResponse.brandId == testedProduct.brand.id
        toLanguageMap(findResponse.names) == testedProduct.names
        asMapInSortedList(findResponse.packs*.barcodes) == asMapInSortedList(testedProduct.packs*.barcodes)
    }

    def "should get product by id"() {
        given: "categories"
        Category allFoods = withCategory(category('All Foods', 'FOOD', ["All Foods"]))
        Category landAnimals = withCategory(category('Land Animals', ["Land Animals"], allFoods.id))
        and: "brands"
        Brand cracow = withBrand(brand(["Kraków", "Cracow"]))
        and: "product"
        Product testedProduct = withProduct(product(
                [en: "product name in english", pl: "Product po polsku"],
                [[EAN: "ean-code-1"], [EAN: "ean-code-2"]],
                landAnimals,
                cracow
        ))

        when:
        def productRequest = invokeGet("/products/$testedProduct.id")
        Map response = contentAsMap(productRequest)
        then:
        httpStatus(productRequest) == 200
        response.id ==~ testedProduct.id
        response.categoryId as Category.CategoryId == testedProduct.category.id
        response.brandId == testedProduct.brand.id
        toLanguageMap(response.names) == testedProduct.names
        asMapInSortedList(response.packs) == asMapInSortedList(testedProduct.packs)
    }

    def "should create & get product by name, language, category and brand by plain request"() {
        given: "categories"
        Category allFoods = withCategory(category('All Foods', 'FOOD', ["All Foods"]))
        Category landAnimals = withCategory(category('Land Animals', ["Land Animals"], allFoods.id))
        Category meatPoultry = withCategory(category('Meat-Poultry', ["Meat-Poultry"], landAnimals.id))
        Category meat = withCategory(category('Meat', ["Meat"], meatPoultry.id))
        withCategory(category('Beef', ["Beef"], meat.id))
        Category pork = withCategory(category('Pork', ["Pork", "Wieprzowina", "Świnina"], meat.id))
        and: "brands"
        Brand cracow = withBrand(brand(["Kraków", "Cracow"]))
        and: "product"
        Product testedProduct = product(
                [en: "product name in english", pl: "Product po polsku"],
                [[EAN: "ean-code-1"], [EAN: "ean-code-2"]],
                pork,
                cracow
        )

        when: "create product"
        def createCategoryByNameRequest = invokePost("/products?plain", asPlainRequestBody(testedProduct))
        Map createResponse = contentAsMap(createCategoryByNameRequest)
        then:
        httpStatus(createCategoryByNameRequest) == 200 //todo status creation
        createResponse.id ==~ /.*/
        createResponse.categoryId as Category.CategoryId == testedProduct.category.id
        createResponse.brandId == testedProduct.brand.id
        toLanguageMap(createResponse.names) == testedProduct.names
        asMapInSortedList(createResponse.packs*.barcodes) == asMapInSortedList(testedProduct.packs*.barcodes)

        when: "get product"
        def findCategoryByNameRequest = invokePost("/products?plain", asPlainRequestBody(testedProduct))
        Map findResponse = contentAsMap(findCategoryByNameRequest)
        then:
        httpStatus(findCategoryByNameRequest) == 200
        findResponse.id ==~ /.*/
        findResponse.categoryId as Category.CategoryId == testedProduct.category.id
        findResponse.brandId == testedProduct.brand.id
        toLanguageMap(findResponse.names) == testedProduct.names
        asMapInSortedList(findResponse.packs*.barcodes) == asMapInSortedList(testedProduct.packs*.barcodes)
    }

    Map toLanguageMap(Map<String, String> toLanguage) {
        toLanguage.collectEntries { key, value -> [LanguageEnum.valueOf(key), value] }
    }

    List asMapInSortedList(def o) {
        new JsonSlurper().parseText(toJson(o)).sort()
    }

    ProductController.CreateOrUpdatePlainRequest asPlainRequestBody(Product product) {
        def category = new ProductController.CreateOrUpdatePlainRequest.Category(product.getCategory().getNames(), [] as Set)
        Set packs = product.packs.collect { p -> new ProductController.CreateOrUpdatePlainRequest.Pack(p.barcodes) }
        return new ProductController.CreateOrUpdatePlainRequest(
                category,
                product.getBrand().getNames(),
                product.getNames(),
                packs
        )
    }

    ProductController.CreateOrUpdateRequest asRequestBody(Product product) {
        def categoryId = new ProductController.CreateOrUpdateRequest.CategoryId(
                product.getCategory().getId().getName(),
                product.getCategory().getId().getType()
        )
        Set packs = product.packs.collect { p -> new ProductController.CreateOrUpdateRequest.Pack(p.barcodes) }
        return new ProductController.CreateOrUpdateRequest(
                categoryId,
                product.getBrand().getId(),
                product.getNames(),
                packs
        )
    }

    Product withProduct(Product product) {
        productRepository.save(product)
    }

    Product product(Map<String, String> names, List<Map<String, String>> packs, Category category, Brand brand) {
        def product = new Product(category, brand, names.find { it }.value, LanguageEnum.valueOf(names.find { it }.key))
        names.each { it -> product.putName(LanguageEnum.valueOf(it.key), it.value) }
        packs.collect { e -> e.collectEntries { k, v -> [BarcodeEnum.valueOf(k), v] } }.each { pack -> product.addPack(pack) }
        return product
    }

    Category category(String idName, String idType, List<String> names) {
        def categoryId = new Category.CategoryId(idName, Category.CategoryId.Type.valueOf(idType))
        new Category(categoryId, null, names)
    }

    Category category(String idName, List<String> names, Category.CategoryId parentId) {
        def parent = categoryRepository.findById(parentId).orElseThrow()
        def id = new Category.CategoryId(idName, parent.getId().type)
        new Category(id, parent, names)
    }

    Category withCategory(Category category) {
        categoryRepository.save(category)
    }

    Brand brand(List<String> names) {
        new Brand(names)
    }

    Brand withBrand(Brand brand) {
        brandRepository.save(brand)
    }

    Map contentAsMap(MvcResult request) {
        new JsonSlurper().parseText(request.response.contentAsString)
    }

    MvcResult invokePost(String url, def body) {
        def bodyJson = toJson(body)
        mockMvc.perform(post(url).content(bodyJson).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn()
    }

    MvcResult invokeGet(String url) {
        mockMvc.perform(get(url)).andReturn()
    }

    Integer httpStatus(MvcResult request) {
        request.response.status
    }

    def extractProperties(obj) {
        obj.getClass()
                .declaredFields
                .findAll { !it.synthetic }
                .collectEntries { field ->
            [field.name, obj."$field.name"]
        }
    }

    def lazyMap(obj) {
        def map = new LazyMap()
        map.putAll(extractProperties(obj))
        return map
    }
}
