package com.tppw.shops.productinformationresource.application.brand


import com.tppw.shops.productinformationresource.domain.model.brand.Brand
import com.tppw.shops.productinformationresource.domain.model.brand.BrandRepository
import groovy.json.JsonSlurper
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:clearDB.sql")
@Testcontainers
class BrandIntegrationSpec extends Specification {

    @Autowired
    protected WebApplicationContext webApplicationContext

    private MockMvc mockMvc

    @Autowired
    private BrandRepository brandRepository

    @Shared
    PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer()
            .withDatabaseName("product-information-test")
            .withUsername("foo")
            .withPassword("secret")

    def setupSpec() {
        ['spring.datasource.url'     : postgreSQLContainer.getJdbcUrl(),
         'spring.datasource.username': postgreSQLContainer.getUsername(),
         'spring.datasource.password': postgreSQLContainer.getPassword()
        ].each { k, v ->
            System.setProperty(k, v)
        }
    }

    @Before
    void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    def "should find brand by name"() {
        given:
        Brand cracow = withBrand(brand(["Wrocław", "Wroclaw"]))
        withBrand(brand(["Brand1", "Marka1"]))

        when: "find brand by name"
        def findBrandByNameRequest = invokePut("/brands/Wroclaw")
        Map response = contentAsMap(findBrandByNameRequest)
        then:
        httpStatus(findBrandByNameRequest) == 200
        response.id == cracow.id
        response.names.sort() == cracow.names.sort()
    }

    def "should create brand by name"() {
        when: "create & find brand by name"
        def findBrandByNameRequest = invokePut("/brands/Warsaw")
        Map response = contentAsMap(findBrandByNameRequest)
        then:
        httpStatus(findBrandByNameRequest) == 200
        response.id ==~ /.*/ //todo digital
        response.names == ["Warsaw"]
    }

    def "should create brand by name with alternative names"() {
        given:
        Brand cracow = brand(["Kraków", "Cracow", "Краків"])

        when: "create & find brand by name"
        def findBrandByNameRequest = invokePut("/brands/Cracow?alternativeNames=Kraków&alternativeNames=Краків")
        Map response = contentAsMap(findBrandByNameRequest)
        then:
        httpStatus(findBrandByNameRequest) == 200
        response.id ==~ /.*/ //todo digital
        response.names.sort() == cracow.names.sort()
    }

    def "should create brand by name and update by alternative names"() {
        given:
        Brand cracow = brand(["Kraków", "Cracow", "Краків"])

        when: "create brand by name"
        def findBrandByNameRequest = invokePut("/brands/Cracow")
        Map response = contentAsMap(findBrandByNameRequest)
        then:
        httpStatus(findBrandByNameRequest) == 200
        response.id ==~ /.*/ //todo digital
        response.names == ["Cracow"]

        when: "create & find brand by name"
        def findBrandByNameWithAlternativeNamesRequest = invokePut("/brands/Cracow?alternativeNames=Kraków&alternativeNames=Краків")
        Map alternativeNamesResponse = contentAsMap(findBrandByNameWithAlternativeNamesRequest)
        then:
        httpStatus(findBrandByNameWithAlternativeNamesRequest) == 200

        alternativeNamesResponse.id ==~ /.*/ //todo digital
        alternativeNamesResponse.names.sort() == cracow.names.sort()
    }

    Map contentAsMap(MvcResult request) {
        new JsonSlurper().parseText(request.response.contentAsString)
    }

    MvcResult invokePut(String url) {
        mockMvc.perform(put(url)).andReturn()
    }

    Integer httpStatus(MvcResult request) {
        request.response.status
    }

    Brand brand(List<String> names) {
        new Brand(names)
    }

    Brand withBrand(Brand brand) {
        brandRepository.save(brand)
    }
}
