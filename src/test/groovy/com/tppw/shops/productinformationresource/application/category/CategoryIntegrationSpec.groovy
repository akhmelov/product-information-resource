package com.tppw.shops.productinformationresource.application.category

import com.tppw.shops.productinformationresource.domain.model.category.Category
import com.tppw.shops.productinformationresource.domain.model.category.CategoryRepository
import groovy.json.JsonSlurper
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.containers.JdbcDatabaseContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.PostgreSQLContainerProvider
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:clearDB.sql")
@Testcontainers
class CategoryIntegrationSpec extends Specification {

    @Autowired
    protected WebApplicationContext webApplicationContext

    @Autowired
    private CategoryRepository categoryRepository

    private MockMvc mockMvc

    @Shared
    PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer()
            .withDatabaseName("product-information-test")
            .withUsername("foo")
            .withPassword("secret")

    def setupSpec() {
        ['spring.datasource.url'     : postgreSQLContainer.getJdbcUrl(),
         'spring.datasource.username': postgreSQLContainer.getUsername(),
         'spring.datasource.password': postgreSQLContainer.getPassword()
        ].each { k, v ->
            System.setProperty(k, v)
        }
    }

    @Before
    void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    def "should find category by name"() {
        given:
        Category allFoods = withCategory(category('All Foods', 'FOOD', ["All Foods"]))
        Category landAnimals = withCategory(category('Land Animals', ["Land Animals"], allFoods.id))
        Category meatPoultry = withCategory(category('Meat-Poultry', ["Meat-Poultry"], landAnimals.id))
        Category meat = withCategory(category('Meat', ["Meat"], meatPoultry.id))
        withCategory(category('Beef', ["Beef"], meat.id))
        Category pork = withCategory(category('Pork', ["Pork", "Wieprzowina", "Świnina"], meat.id))

        when: "find category by name and parents"
        def findCategoryByNameRequest = invokeGet("/categories/Pork")
        Map response = contentAsMap(findCategoryByNameRequest)
        then:
        httpStatus(findCategoryByNameRequest) == 200
        response.id as Category.CategoryId == pork.id
        response.parentId as Category.CategoryId == pork.parent.id
        response.names.sort() == pork.names.sort()
    }

    Category category(String idName, String idType, List<String> names) {
        def categoryId = new Category.CategoryId(idName, Category.CategoryId.Type.valueOf(idType))
        new Category(categoryId, null, names)
    }

    Category category(String idName, List<String> names, Category.CategoryId parentId) {
        def parent = categoryRepository.findById(parentId).orElseThrow()
        def id = new Category.CategoryId(idName, parent.getId().type)
        new Category(id, parent, names)
    }

    Category withCategory(Category category) {
        categoryRepository.save(category)
    }

    Map contentAsMap(MvcResult request) {
        new JsonSlurper().parseText(request.response.contentAsString)
    }

    MvcResult invokeGet(String url) {
        mockMvc.perform(get(url)).andReturn()
    }

    Integer httpStatus(MvcResult request) {
        request.response.status
    }
}
